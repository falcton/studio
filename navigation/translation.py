from modeltranslation.translator import translator, TranslationOptions

from navigation.models import TranslatableTreeItem


class TreeItemTranslationOptions(TranslationOptions):
    fields = ('title', 'hint', 'description', )

translator.register(TranslatableTreeItem, TreeItemTranslationOptions)
