from sitetree.admin import TreeItemAdmin, override_item_admin
from modeltranslation.admin import TranslationAdmin


class TranslatableTreeAdmin(TreeItemAdmin, TranslationAdmin):
    pass

override_item_admin(TranslatableTreeAdmin)
