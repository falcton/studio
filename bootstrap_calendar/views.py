import json
import pytz
from datetime import datetime

from django.http import HttpResponse
from django.conf import settings

from blog.models import Post


def show_posts(request):
    year = request.GET.get('year', datetime.today().year)
    month = request.GET.get('month', datetime.today().month)
    raw_posts = Post.objects.filter(is_draft=False, published__year=year, published__month=month)
    posts = [{'date': x.published.astimezone(pytz.timezone(settings.TIME_ZONE)).strftime('%Y-%m-%d')} for x in raw_posts]
    return HttpResponse(json.dumps(posts), content_type='application/json')
