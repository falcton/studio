# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('mptt_comments', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mpttcomment',
            options={'ordering': ('tree_id', 'lft'), 'verbose_name': 'Threaded comment', 'verbose_name_plural': 'Threaded comments'},
        ),
        migrations.RemoveField(
            model_name='mpttcomment',
            name='last_child',
        ),
        migrations.RemoveField(
            model_name='mpttcomment',
            name='tree_path',
        ),
        migrations.AddField(
            model_name='mpttcomment',
            name='level',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mpttcomment',
            name='lft',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mpttcomment',
            name='rght',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mpttcomment',
            name='tree_id',
            field=models.PositiveIntegerField(default=0, editable=False, db_index=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='mpttcomment',
            name='content_type',
            field=models.ForeignKey(verbose_name='content type', related_name='content_type_set_for_mpttcomment', to='contenttypes.ContentType'),
        ),
        migrations.AlterField(
            model_name='mpttcomment',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, related_name='children', to='mptt_comments.MPTTComment', null=True),
        ),
        migrations.AlterField(
            model_name='mpttcomment',
            name='user',
            field=models.ForeignKey(blank=True, verbose_name='user', related_name='mpttcomment_comments', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
