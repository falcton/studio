from django.conf.urls import url

from mptt_comments.views import submit

urlpatterns = [
    url(r'^submit/$', submit, name='submit'),
]
