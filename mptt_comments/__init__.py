from mptt_comments.models import MPTTComment
from mptt_comments.forms import MPTTCommentForm


def get_model():
    return MPTTComment


def get_form():
    return MPTTCommentForm
