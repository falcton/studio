from django.contrib.flatpages.models import FlatPage
from modeltranslation.translator import translator, TranslationOptions

from blog.models import Post


class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'text', )

translator.register(Post, PostTranslationOptions)


class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )

translator.register(FlatPage, FlatPageTranslationOptions)
