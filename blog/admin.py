from django.db import models
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from tinymce.widgets import AdminTinyMCE
from modeltranslation.admin import TranslationAdmin

from blog.models import Post, Tag, Subscription


class TagAdmin(admin.ModelAdmin):
    pass

admin.site.register(Tag, TagAdmin)


class PostAdmin(TranslationAdmin):
    fieldsets = (
        (None, {
            'fields': ('author', 'title', 'text', 'tags', 'is_draft', ),
        }),
        (_('Repost control'), {
            'classes': ('collapse', ),
            'fields': ('repost_status', 'repost_attempts', 'repost_url', 'repost_itemid', ),
        }),
    )

admin.site.register(Post, PostAdmin)


class SubscriptionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Subscription, SubscriptionAdmin)


class CustomFlatPageAdmin(TranslationAdmin):
    list_display = ('title', 'url', )
    fieldsets = (
        (None, {
            'fields': ('url', 'title', 'content', 'sites', )
        }),
        (_('Advanced options'), {
            'classes': ('collapse', ),
            'fields': ('enable_comments', 'registration_required', 'template_name', ),
        }),
    )
    formfield_overrides = {
        models.TextField: {
            'widget': AdminTinyMCE(),
        },
    }
    list_filter = ('sites', 'enable_comments', 'registration_required', )
    search_fields = ('url', 'title', )

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, CustomFlatPageAdmin)
