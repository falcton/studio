# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0013_auto_20151110_0758'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='subscribed',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2015, 11, 10, 16, 35, 51, 100241, tzinfo=utc), verbose_name='Subscribed at'),
            preserve_default=False,
        ),
    ]
