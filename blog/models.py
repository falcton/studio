from datetime import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from tinymce.models import HTMLField

from studio.celery import app
from mptt_comments.models import MPTTComment
from mptt_comments.signals import comment_was_posted

POST_REPOST_STATUS = (
    (0, 'Do not repost'),
    (1, 'Scheduled for repost'),
    (2, 'Reposted'),
    (3, 'Failed to repost'),
)


class Tag(models.Model):
    name = models.CharField(_('Name'), max_length=255)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __str__(self):
        return self.name


class Post(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Author'))
    tags = models.ManyToManyField(Tag, verbose_name=_('Tags'), related_name='posts', blank=True)
    created = models.DateTimeField(_('Created'), auto_now_add=True)
    edited = models.DateTimeField(_('Edited'), auto_now=True)
    published = models.DateTimeField(_('Published'), blank=True, null=True)
    title = models.CharField(_('Title'), max_length=255)
    text = HTMLField(_('Text'))
    is_draft = models.BooleanField(_('Draft'), default=True)
    repost_status = models.SmallIntegerField(_('Repost status'), choices=POST_REPOST_STATUS, default=1)
    repost_attempts = models.SmallIntegerField(_('Failed repost attempts'), default=0)
    repost_url = models.URLField(_('Reposted to'), blank=True, null=True)
    repost_itemid = models.PositiveIntegerField(_('Repost ID'), blank=True, null=True)

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['-published', '-created', ]

    def save(self, *args, **kwargs):
        """
        Allows post publishing and republishing.
        """
        if self.is_draft:
            self.published = None
        else:
            self.published = datetime.now()
        super().save()

    def get_absolute_url(self):
        return reverse('blog:post', args=[self.id])

    def __str__(self):
        return self.title


@receiver(post_save)
def repost(sender, instance, **kwargs):
    """
    Schedule post ID for repost.
    """
    if sender is Post and instance.repost_status == 1:
        app.send_task('blog.tasks.do_repost_to_lj', [instance.id, ], countdown=5)


class PostRu(Post):
    """
    Search proxy for Post.
    """
    class Meta:
        proxy = True
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['-published', '-created', ]


class FlatPageRu(FlatPage):
    """
    Search proxy for FlatPage.
    """
    class Meta:
        proxy = True
        db_table = 'django_flatpage'
        verbose_name = _('flat page')
        verbose_name_plural = _('flat pages')
        ordering = ('url', )


class Subscription(models.Model):
    """
    Let registered users subscribe to posts to receive comments and updates by mail.
    """
    subscriber = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'), related_name='subscriptions')
    post = models.ForeignKey(Post, verbose_name=_('Post'), related_name='subscriptions')
    subscribed = models.DateTimeField(_('Subscribed at'), auto_now_add=True)

    class Meta:
        unique_together = ('subscriber', 'post', )
        verbose_name = _('Subscription')
        verbose_name_plural = _('Subscriptions')

    def clean_fields(self, exclude=None):
        # do not let user subscribe if his email not set
        for field in self._meta.fields:
            if field.name == 'subscriber' and not self.subscriber.email:
                raise ValidationError({
                    'subscriber': _('You must fill in your email to subscribe.'),
                })
        super().clean_fields(exclude)

    def __str__(self):
        return '<{0}>: «{1}»'.format(self.subscriber.email, self.post.title if self.post.title else 'No title')


@receiver(comment_was_posted)
def notify_subscribers(sender, comment, **kwargs):
    """
    Schedule the delivery of notifications for the subscribers.
    """
    if sender is MPTTComment:
        for subscription in comment.content_object.subscriptions.all():
            if subscription.subscriber.email:
                app.send_task('blog.tasks.do_notify_subscribers',
                              [
                                  subscription.subscriber.id,
                                  comment.id,
                              ],
                              countdown=5)
