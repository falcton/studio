import pytz
from datetime import datetime

from django.conf import settings
from django.db import IntegrityError
from django.db.models import Min, Max
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.translation import ugettext_lazy as _
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from blog.models import Post, Subscription


def get_calendar_range():
    """
    Returns calendar display range.
    """
    tz = pytz.timezone(settings.TIME_ZONE)
    result = {
        'start_date': Post.objects.filter(is_draft=False).aggregate(Min('published'))['published__min'].astimezone(tz),
        'end_date': Post.objects.filter(is_draft=False).aggregate(Max('published'))['published__max'].astimezone(tz),
    }
    return result


def post_list(request):
    """
    View list of posts.
    """
    tz = pytz.timezone(settings.TIME_ZONE)
    ctx = dict()
    query = dict(is_draft=False)
    # set calendar dates display range
    ctx.update(**get_calendar_range())
    # filter by date
    date_str = request.GET.get('date', None)
    if date_str:
        date = tz.localize(datetime.strptime(date_str, '%Y-%m-%d'))
        query['published__year'] = date.year
        query['published__month'] = date.month
        query['published__day'] = date.day
        ctx['filter_date'] = date
        ctx['current_date'] = date
    else:
        ctx['filter_date'] = None
        ctx['current_date'] = datetime.now(tz)
    # check current calendar date against range
    if ctx['current_date'] > ctx['end_date']:
        ctx['current_date'] = ctx['end_date']
    # filter by tag
    tag_str = request.GET.get('tag', None)
    if tag_str:
        query['tags__name'] = tag_str
        ctx['tag'] = tag_str
    posts_obj = Post.objects.filter(**query)
    if len(posts_obj) == 1:
        return redirect('blog:post', post_id=posts_obj[0].id)
    # paginate result
    posts_per_page = getattr(settings, 'BLOG_POSTS_PER_PAGE', 10)
    page = request.GET.get('p')
    paginator = Paginator(posts_obj, posts_per_page)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    ctx['posts'] = posts
    return render(request, template_name='blog/post_list.html', context=ctx)


def post_detail(request, post_id):
    """
    View single post.

    :param post_id: Post ID
    """
    tz = pytz.timezone(settings.TIME_ZONE)
    post = get_object_or_404(Post, id=post_id)
    ctx = {
        'post': post,
        'filter_date': post.published.astimezone(tz),
    }
    ctx.update(**get_calendar_range())
    return render(request, template_name='blog/post_detail.html', context=ctx)


@login_required
@api_view(['GET', 'POST', 'DELETE', ])
def manage_subscriptions(request, post_id):
    """
    View, create or delete a subscription to the post.

    :param post_id: the post ID.
    """
    post = get_object_or_404(Post, id=post_id)
    if request.method == 'POST':
        if request.user.email:
            subscription = Subscription(subscriber=request.user, post=post)
            try:
                subscription.save()
            except IntegrityError:
                return Response({
                    'message': _('You’re already subscribed to this object.'),
                }, status=status.HTTP_201_CREATED)
        else:
            return Response({
                'message': _('You must fill in your email to subscribe.'),
            }, status=status.HTTP_400_BAD_REQUEST)
    else:
        subscription = get_object_or_404(Subscription, subscriber=request.user, post=post)
        if request.method == 'DELETE':
            subscription.delete()
    return Response({
        'message': 'OK',
    })


@login_required
def subscriptions(request):
    ctx = {
        'subscriptions': Subscription.objects.filter(subscriber=request.user)
    }
    return render(request, template_name='account/subscriptions.html', context=ctx)
