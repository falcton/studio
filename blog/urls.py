from django.conf.urls import url

from blog.views import post_detail, post_list, manage_subscriptions

urlpatterns = [
    url(r'^(?P<post_id>\d+)/$', post_detail, name='post'),
    url(r'^subscribe/(?P<post_id>\d+)/$', manage_subscriptions, name='subscribe'),
    url(r'^$', post_list, name='index'),
]
