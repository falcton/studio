from django.conf import settings
from django import forms
from django.utils.translation import get_language, ugettext_lazy as _
from haystack import routers
from haystack.forms import ModelSearchForm, model_choices
from haystack.constants import DEFAULT_ALIAS
from haystack.query import SearchQuerySet


def get_alias():
    language_code = get_language()
    if language_code is not None and language_code[:2] != 'en':
        alias = '{0}_{1}'.format(DEFAULT_ALIAS, language_code)
        if alias in settings.HAYSTACK_CONNECTIONS.keys():
                return alias
    return DEFAULT_ALIAS


class MultilingualRouter(routers.BaseRouter):
    """
    Each language have its own connection, named ‘default_’ + language code.
    Except ‘en’, which is ‘default’.
    """
    def for_read(self, **hints):
        return get_alias()

    def for_write(self, **hints):
        return get_alias()


class MultiIndexModelSearchForm(ModelSearchForm):
    """
    Had to override this, since standard implementation assumes DEFAULT_ALIAS.
    """
    def __init__(self, *args, **kwargs):
        super(ModelSearchForm, self).__init__(*args, **kwargs)
        self.fields['models'] = forms.MultipleChoiceField(choices=model_choices(using=get_alias()),
                                                          required=False,
                                                          label=_('Search In'),
                                                          widget=forms.CheckboxSelectMultiple)
        self.searchqueryset = SearchQuerySet(using=get_alias())
