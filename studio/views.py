import pytz
from datetime import datetime

from django.conf import settings
from django.http import Http404, HttpResponse, HttpResponsePermanentRedirect, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.db.models import Min, Max
from django.contrib import messages
from django.contrib.sites.models import get_current_site
from django.contrib.flatpages.models import FlatPage
from django.contrib.auth.decorators import login_required
from haystack.views import SearchView
from account.views import LoginView
from social.backends.utils import load_backends
from avatar.models import Avatar
from avatar.signals import avatar_updated
from avatar.views import _get_avatars
from avatar.forms import UploadAvatarForm

from blog.models import Post

DEFAULT_FLATPAGE_TEMPLATE = 'flatpages/default.html'


def home(request):
    tz = pytz.timezone(settings.TIME_ZONE)
    ctx = {
        'start_date': Post.objects.filter(is_draft=False).aggregate(Min('published'))['published__min'].astimezone(tz),
        'end_date': Post.objects.filter(is_draft=False).aggregate(Max('published'))['published__max'].astimezone(tz),
        'filter_date': datetime.now(tz=tz),
        'last_posts': Post.objects.filter(is_draft=False)[0:3],
    }
    return render(request, template_name='home.html', context=ctx)


class CustomSearchView(SearchView):
    def extra_context(self):
        return dict(last_posts=Post.objects.filter(is_draft=False)[0:3])


class SocialLoginView(LoginView):
    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update(dict(available_backends=load_backends(settings.AUTHENTICATION_BACKENDS)))
        return ctx


def flatpage(request, url):
    """
    Flatpage renderer with some additional context.
    """
    if not url.startswith('/'):
        url = '/' + url
    site_id = get_current_site(request).id
    try:
        flatpage_obj = get_object_or_404(FlatPage, url=url, sites=site_id)
    except Http404:
        if not url.endswith('/') and settings.APPEND_SLASH:
            url += '/'
            flatpage_obj = get_object_or_404(FlatPage, url=url, sites=site_id)
            return HttpResponsePermanentRedirect('%s/' % request.path)
        else:
            raise
    if flatpage_obj.registration_required and not request.user.is_authenticated():
        from django.contrib.auth.views import redirect_to_login
        return redirect_to_login(request.path)
    if flatpage_obj.template_name:
        template = loader.select_template((flatpage_obj.template_name, DEFAULT_FLATPAGE_TEMPLATE))
    else:
        template = loader.get_template(DEFAULT_FLATPAGE_TEMPLATE)
    flatpage_obj.title = mark_safe(flatpage_obj.title)
    flatpage_obj.content = mark_safe(flatpage_obj.content)
    response = HttpResponse(template.render({
        'flatpage': flatpage_obj,
        'last_posts': Post.objects.filter(is_draft=False)[0:3],
    }, request))
    return response


@login_required
def avatars(request):
    """
    Renders list of avatars for the current user with upload form and controls.
    """
    avatar, avatar_list = _get_avatars(request.user)
    upload_form = UploadAvatarForm(request.POST or None, request.FILES or None, user=request.user)
    # a little hack to make upload control appearance clearer
    upload_form.fields['avatar'].label = _('Choose an image to upload:')
    if request.method == "POST" and 'avatar' in request.FILES:
        if upload_form.is_valid():
            avatar = Avatar(user=request.user, primary=True)
            image_file = request.FILES['avatar']
            avatar.avatar.save(image_file.name, image_file)
            avatar.save()
            messages.success(request, _('Avatar is uploaded.'))
            avatar_updated.send(sender=Avatar, user=request.user, avatar=avatar)
    ctx = {
        'avatar': avatar,
        'avatars': avatar_list,
        'upload_avatar_form': upload_form,
    }
    return render(request, 'avatar/avatars.html', context=ctx)


@login_required
def delete_avatar(request, avatar_id):
    avatar = get_object_or_404(Avatar, id=avatar_id)
    if avatar.user == request.user:
        avatar.delete()
    return HttpResponseRedirect('avatars')


@login_required
def set_primary_avatar(request, avatar_id):
    avatar = get_object_or_404(Avatar, id=avatar_id)
    if avatar.user == request.user:
        avatar.primary = True
        avatar.save()
    return HttpResponseRedirect('avatars')
