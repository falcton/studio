from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.conf.urls import i18n as i18n_urls
from rosetta import urls as rosetta_urls
from grappelli import urls as grappelli_urls
from filebrowser.sites import site
from tinymce import urls as tinymce_urls
from mce_filebrowser import urls as mce_filebrowser_urls
from account import urls as account_urls
from social.apps.django_app import urls as social_urls
from mptt_comments import urls as comments_urls
from studio.views import CustomSearchView, avatars, set_primary_avatar, delete_avatar

from blog import urls as blog_urls
from blog.feeds import BlogFeed
from blog.views import subscriptions
from bootstrap_calendar import urls as calendar_urls
from studio.views import home, SocialLoginView, flatpage
from studio.search.search import MultiIndexModelSearchForm
from studio.sitemaps import sitemaps

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^i18n/', include(i18n_urls)),
    url(r'^rosetta/', include(rosetta_urls)),
    url(r'^search/', CustomSearchView(form_class=MultiIndexModelSearchForm), name='search'),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^grappelli/', include(grappelli_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include(tinymce_urls)),
    url(r'^mce_filebrowser/', include(mce_filebrowser_urls)),
    url(r"^account/login/$", SocialLoginView.as_view(), name="account_login"),
    url(r'^account/avatars/delete/(?P<avatar_id>\d+)$', delete_avatar, name='delete-avatar'),
    url(r'^account/avatars/primary/(?P<avatar_id>\d+)$', set_primary_avatar, name='primary-avatar'),
    url(r'^account/avatars/', avatars, name='avatars'),
    url(r'^account/subscriptions/$', subscriptions, name='subscriptions'),
    url(r'^account/', include(account_urls)),
    url(r'^social/', include(social_urls, namespace='social')),
    url(r'^mptt_comments/', include(comments_urls, namespace='comments')),
    url(r'^blog/feed/$', BlogFeed(), name='blog-feed'),
    url(r'^blog/', include(blog_urls, namespace='blog')),
    url(r'^calendar/', include(calendar_urls, namespace='calendar')),
    url(r'^$', home, name='home'),
    url(r'^(?P<url>.*/)$', flatpage),
]

if getattr(settings, 'DEBUG', None):
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
