from django.contrib import admin

from modeltranslation.admin import TranslationAdmin

from text_blocks.models import TextBlock


class TextBlockAdmin(TranslationAdmin):
    fields = ['slug', 'header', 'content', ]

admin.site.register(TextBlock, TextBlockAdmin)
